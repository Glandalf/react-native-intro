// eslint-disable-next-line no-undef
module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'eslint:recommended',
    // 'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'object-curly-spacing': ['error', 'always', ],
    'comma-dangle': ['error', {
        'arrays': 'always',
        'objects': 'always',
        'imports': 'always',
        'exports': 'never',
        'functions': 'never',
    },],
    'no-trailing-spaces': 'error',
    'quotes': ['error', 'single', { 'allowTemplateLiterals': true, },],
  },
};
