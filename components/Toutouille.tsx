import React from 'react'
import { View, Text, Button, TextInput, } from 'react-native'

interface ToutouilleProps {
    name: string,
    level?: number
}


export default function Toutouille(props: ToutouilleProps) {
    // Déclaration non réactive :
    // let name = 'Bulbiflex'
    const [name, setName,] = React.useState(props.name)
    // Déclaration non réactive :
    // let level = 24
    const [level, setLevel,] = React.useState(props.level ?  props.level : 1)
    const attaques = ['bubule', 'dodo', 'coucou', 'pioupiou',]

    function levelUp() {
        setLevel(level + 1)
        console.log(level);
    }

    return (
        <View>
            <Text style={{ fontSize: 25, }}>Je suis {name}</Text>
            <Text>
                {name} est un pokemon. Il a des attaques et il est de niveau {level}.
            </Text>
            { level < 30 && <Text style={{ color: 'red', }}>Frajil</Text> }
            { level >= 30 && level < 35 && <Text style={{ color: 'orange', }}>Mouaiiis</Text> }
            { level >= 35 && level < 40 && <Text style={{ color: 'green', }}>Respect</Text> }
            {
                attaques.map((attaque, index) => <Text key={attaque}>Attaque {index + 1} : {attaque}</Text>)
            }

            { /*<Button title="lvlheup" onPress={() => {
                setLevel(level + 1)
                console.log(level)
            }}/> */ }
            <Button title="lvlheup" onPress={levelUp}/>
            <TextInput value={name} onChangeText={newValue => setName(newValue)}/>
        </View>
    )
}

